# yatranslate

Go Yandex Translate API

Documentation: https://cloud.yandex.ru/docs/translate/quickstart

## Getting started
```
package main

import (
	"fmt"
	"time"

	"gitlab.com/mazhigali/yatranslate"
)

func main() {
	tr := yatranslate.NewYandexTranslator("folderID", "oauth-token", 10*time.Minute)
	text := "Привет, мир!"
	yandexTranslation, err := tr.TranslateByYandex("en", text, "")
	if err != nil {
		return
	}
	fmt.Println(yandexTranslation)
}

```

TODO https://cloud.yandex.ru/docs/translate/concepts/glossary
